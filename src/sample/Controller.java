package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

public class Controller implements Initializable {
    public Button btnWorking;
    public ProgressBar pbWorking;
    public ImageView iv;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //pbWorking.setProgress(0.6f);
        iv.setVisible(false);
        pbWorking.setVisible(false);
        iv.setImage(new Image(getClass().getResource("/resources/1.jpg").toString()));
    }
    //Task

    public void onWorking(ActionEvent actionEvent) {
        iv.setImage(new Image(getClass().getResource("/resources/1.jpg").toString()));
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        //pbWorking.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        Task task = new Task<Void>(){


            @Override
            protected Void call() throws Exception {
                IntStream.range(0,3).forEach(e -> {
                    timeline.getKeyFrames().add(
                            new KeyFrame(Duration.seconds(e + 1), new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    iv.setImage(new Image(getClass().getResource("/resources/".concat(String.valueOf(e+2)).concat(".jpg")).toString()));
                                }
                            })
                    );
                });

//                final int max = 100;
//                pbWorking.setVisible(true);
//                //pbWorking.setProgress(0f);
//                for (int i = 0 ; i <=max ; i++ ){
//                    Thread.sleep(100);
//                    updateProgress(i,max);
//                }
                Thread.sleep(4000);
                return null;
            }
            @Override
            protected void scheduled(){////ing
                iv.setImage(new Image(getClass().getResource("/resources/1.jpg").toString()));
                super.scheduled();
                pbWorking.setVisible(true);
                timeline.play();
                iv.setVisible(true);

            }
            @Override
            protected void succeeded(){
                iv.setImage(new Image(getClass().getResource("/resources/1.jpg").toString()));
                super.succeeded();
                pbWorking.setVisible(false);
                timeline.stop();
                iv.setVisible(false);
            }
            @Override
            protected void failed(){
                super.failed();
                //pbWorking.setVisible(false);
            }

        };


        pbWorking.progressProperty().bind(task.progressProperty());
        //pbWorking.setVisible(true);
        new Thread(task).start();

//        List<String> imagepath = new ArrayList<>();
//        for (int i = 0 ; i < 4 ; i++ ){
//            imagepath.add("/resources/".concat(String.valueOf(i+1)).concat(".jpg"));
//        }


//
//        for (int i = 0 ; i < 4 ; i++ ){
//            int j = i;
//            timeline.getKeyFrames().add(
//                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
//                        @Override
//                        public void handle(ActionEvent event) {
//                            iv.setImage(new Image(getClass().getResource("/resources/".concat(String.valueOf(j+1)).concat(".jpg")).toString()));
//                        }
//                    })
//            );
//        }





    }
}
